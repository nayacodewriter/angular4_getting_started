import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CanActivate} from "@angular/router";

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AlwaysAuthGuard } from './service/authguard'

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'login',  component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AlwaysAuthGuard] },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}