import {CanActivate} from "@angular/router";
import { Injectable } from '@angular/core';
import { Router }   from '@angular/router';

@Injectable()

export class AlwaysAuthGuard implements CanActivate {
    constructor(
   private router: Router     ) { }
  canActivate() {
    console.log("AlwaysAuthGuard");
        this.router.navigate(['/login']);

    return false;
    
  }
}