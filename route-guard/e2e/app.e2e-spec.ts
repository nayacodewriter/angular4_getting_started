import { AngularpouchPage } from './app.po';

describe('angularpouch App', () => {
  let page: AngularpouchPage;

  beforeEach(() => {
    page = new AngularpouchPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
